var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
require('date-utils');




app.get("/",function(req, res){
  res.sendfile("client.html");
});

var count=1;
var love_num=0;
io.on('connection', function(socket){
  var newDate=new Date();
  var time = newDate.toFormat('MM-DD HH24:MI:SS')
  console.log(time,' : user connected: ', socket.id);
  var name = "user";
  io.to(socket.id).emit('change name',name);
  // io.to(socket.id).emit('love num',love_num,time);

  socket.on('disconnect', function(){
    var newDate=new Date();
    var time = newDate.toFormat('MM-DD HH24:MI:SS')
    console.log(time,'user disconnected: ', socket.id);
  });
  
  socket.on('send message', function(name,text){
    var newDate=new Date();
    var time = newDate.toFormat('MM-DD HH24:MI:SS')
    var msg = name + ' : ' + text;
    console.log(time,' : ',msg);
    io.emit('receive message', msg);
  });

  socket.on('give love', function(name){
    love_num++;
    var newDate=new Date();
    var time = newDate.toFormat('MM-DD HH24:MI:SS')
    console.log(time,' : ',name,' gave me a love ');
    io.emit('love num', love_num,time);
  });

});

http.listen('3000', function(){
  console.log("server on!");
});

